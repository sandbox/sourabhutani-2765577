*********************************************************
-----------------------MailonLogin-------------------
*********************************************************

Introduction
------------
This module is use to send email after users logged in on website.
But new thing is that by using this module admin can choose users
list by this path [admin/config/mailonlogin] to send them mail.

Requirements
------------


 Installation & Use
-------------------
* Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

Limitation
------------
